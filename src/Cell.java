import javafx.scene.paint.Color;

public class Cell {
    private Color color;
    private boolean state;
    private boolean radiusMark;
    public boolean border;
    public boolean inclusion;
    public boolean substructure;
    public boolean selected;
    public boolean recr;
    public int energy;

    public int seedid;

    private int id;

    public Cell(){
        this.color = Color.BLACK;
        this.state = false;
        this.radiusMark=false;
        this.border=false;
        this.substructure=false;
        this.inclusion=false;
        this.selected=false;
        this.recr=false;
        this.energy=0;


    }
    public void setdstate(int r)
    {
        this.seedid=r;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

}
