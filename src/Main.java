import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class Main  extends Application {

    private static boolean colorborder=false;
    private static int appHeight=600;
    private static int appWidth=600;
    private static  int pixelSize=10;
    private static int logicNeighbourhood=0;
    private static boolean logicPeriodic=false;
    private static int numberOfFirstSeeds=150;
    private static int numberOfInclusions=150;
    private static int inclusionSize=0;
    private static int seedRule=0;
    private static int typeOfInclusion=2;
    private static int shapeOfInclusion=2;
    private static int probability;
    private static int energyJ=1;
    private static int nucmod=1; //1 constant, 2 - inc, 3 - at the begin
    private static int nucloc=1; // 1 GB, 2 anywhere
    private static boolean heterogenousdistribution=false;

    public static boolean changed=false;
    public static class lastchanged{public static int x; public static int y;};
    private static boolean gameState=true;
    private static int r=10;
    public static boolean odswiezindex=true;
    public static int ilindeksow;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Grain Growth");
        Group root = new Group();

        javafx.scene.canvas.Canvas canvas = new javafx.scene.canvas.Canvas(appWidth, appHeight);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        canvas.setLayoutY(0);
        canvas.setLayoutX(600);
        int buttonSpace=40;





        Button btn=new Button();
        btn.setText("Start");
        btn.setLayoutX(2);
        btn.setLayoutY(0);
        Button btnimg=new Button();
        btnimg.setText("IMG EXP");
        btnimg.setLayoutX(2+buttonSpace+0.5);
        btnimg.setLayoutY(0);
        Button btndist=new Button();
        btndist.setText("ENERGY DIST.");
        btndist.setLayoutX(3*buttonSpace);
        btndist.setLayoutY(0);
        Button btnimgimp=new Button();
        btnimgimp.setText("IMG IMP");
        btnimgimp.setLayoutX(100+buttonSpace+0.5);
        btnimgimp.setLayoutY(0);
        Button btn3=new Button();
        btn3.setText("Restart");
        btn3.setLayoutX(2+buttonSpace+0.5);
        btn3.setLayoutY(0);

        //textfieldy
        Label tf1Label=new Label("Amount:");
        TextField textField1=new TextField("30");
        Label tf2Label=new Label("Inclusion Size");
        TextField textField2=new TextField("10");
        Label tf3Label=new Label("Height");
        TextField textField3=new TextField("200");
        Label tf4Label=new Label("Width");
        TextField textField4=new TextField("200");
        Label tfLabelprob=new Label("X(Prb.):");
        TextField textFieldprob=new TextField("10");






        tf3Label.setLayoutX(200*1+5);
        tf3Label.setLayoutY(buttonSpace);
        textField3.setLayoutX(200*1+5);
        textField3.setLayoutY(tf3Label.getLayoutY()+buttonSpace);
        Label jlabel=new Label("J");
        TextField jField=new TextField("1");
        jlabel.setLayoutX(400);
        jlabel.setLayoutY(tf3Label.getLayoutY());
        jField.setLayoutX(400);
        jField.setLayoutY(textField3.getLayoutY());
        Label Nlabel=new Label("MC STEPS");
        TextField nField=new TextField("10");
        Nlabel.setLayoutX(400);
        Nlabel.setLayoutY(jField.getLayoutY()+buttonSpace);
        nField.setLayoutX(400);
        nField.setLayoutY(Nlabel.getLayoutY()+buttonSpace);
        Label nucletionmodellabel=new Label("Apply Nucleation Model:");
        nucletionmodellabel.setLayoutX(400);
        nucletionmodellabel.setLayoutY(nField.getLayoutY()+buttonSpace);
        TextField nuclmodelfield=new TextField("Constant");
        nuclmodelfield.setLayoutX(400);
        nuclmodelfield.setLayoutY(nucletionmodellabel.getLayoutY()+buttonSpace);
        final ContextMenu cmenunucmod=new ContextMenu();
        MenuItem constant=new MenuItem("Constant");
        MenuItem beginnig=new MenuItem("At the beginning");
        MenuItem inc=new MenuItem("Increasing");
        cmenunucmod.getItems().addAll(constant,beginnig,inc);
        constant.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuclmodelfield.setText("Constant");
                nucmod=1;
            }
        });
        beginnig.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuclmodelfield.setText("At the beginning");
                nucmod=3;
            }
        });

        inc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuclmodelfield.setText("Increasing");
                nucmod=2;
            }
        });
        nuclmodelfield.setContextMenu(cmenunucmod);
        Label nucwherelabel=new Label("Location of nucleons");
        nucwherelabel.setLayoutX(400);
        nucwherelabel.setLayoutY(nuclmodelfield.getLayoutY()+buttonSpace);

        TextField nuclwherefield=new TextField("GB");
        final ContextMenu nuclocmen=new ContextMenu();
        MenuItem GB=new MenuItem("GB");
        MenuItem anywhere=new MenuItem("Anywhere");
        nuclocmen.getItems().addAll(GB,anywhere);
        GB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuclwherefield.setText("GB");
                nucloc=1;
            }
        });
        anywhere.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nuclwherefield.setText("Anywhere");
                nucloc=2;
            }
        });
        nuclwherefield.setContextMenu(nuclocmen);
        nuclwherefield.setLayoutX(400);
        nuclwherefield.setLayoutY(nucwherelabel.getLayoutY()+buttonSpace);
        Label numberofnucleons=new Label("Number of nucleons");
        numberofnucleons.setLayoutX(400);
        numberofnucleons.setLayoutY(nuclwherefield.getLayoutY()+buttonSpace);
        TextField numberofnuclField=new TextField("10");
        numberofnuclField.setLayoutX(400);
        numberofnuclField.setLayoutY(numberofnucleons.getLayoutY()+buttonSpace);

        tf4Label.setLayoutX(0);
        tf4Label.setLayoutY(tf3Label.getLayoutY());
        textField4.setLayoutX(0);
        textField4.setLayoutY(tf4Label.getLayoutY()+buttonSpace);
        tf1Label.setLayoutX(0);
        tf1Label.setLayoutY(textField4.getLayoutY()+buttonSpace);
        textField1.setLayoutX(0);
        textField1.setLayoutY(tf1Label.getLayoutY()+buttonSpace);
        tfLabelprob.setLayoutX(205);
        tfLabelprob.setLayoutY(textField4.getLayoutY()+buttonSpace);
        textFieldprob.setLayoutX(205);
        textFieldprob.setLayoutY(tfLabelprob.getLayoutY()+buttonSpace);
        TextField temptext=new TextField("Moora");
        logicNeighbourhood=0;
        final ContextMenu c1=new ContextMenu();
        MenuItem moore=new MenuItem("Moora");
        MenuItem neumann=new MenuItem("von Neumanna");
        MenuItem moore2=new MenuItem("Grain Control Moore");
        c1.getItems().addAll(moore,neumann,moore2);
        moore.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                temptext.setText("Moora");
                logicNeighbourhood=0;
            }
        });
        neumann.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                temptext.setText("von Neumanna");
                logicNeighbourhood=1;
            }
        });

        moore2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                temptext.setText("Grain Control Moore");
                logicNeighbourhood=2;
            }
        });

        Label neighLabel=new Label("Neighberhood:");
        neighLabel.setLayoutX(0);
        neighLabel.setLayoutY(textField1.getLayoutY()+buttonSpace);
        temptext.setContextMenu(c1);
        temptext.setLayoutX(0);
        temptext.setLayoutY(neighLabel.getLayoutY()+buttonSpace);


        //periodic
        Label periodicLabel=new Label("Boundary Conditions");
        periodicLabel.setLayoutX(225);
        periodicLabel.setLayoutY(neighLabel.getLayoutY());

        final ToggleGroup group2 = new ToggleGroup();

        RadioButton rb6=new RadioButton("Absorbing");
        rb6.setToggleGroup(group2);
        rb6.setSelected(true);
        rb6.setLayoutX(225);
        rb6.setLayoutY(periodicLabel.getLayoutY()+buttonSpace);

        Label inclusionsLabel=new Label("Inclusions");
        inclusionsLabel.setLayoutX(225);
        inclusionsLabel.setLayoutY(rb6.getLayoutY()+buttonSpace);

        final ToggleGroup group3 = new ToggleGroup();
        final ToggleGroup group4 = new ToggleGroup();

        RadioButton rb7=new RadioButton("After Growth");
        rb7.setToggleGroup(group3);
        rb7.setSelected(true);
        rb7.setLayoutX(225);
        rb7.setLayoutY(inclusionsLabel.getLayoutY()+buttonSpace);

        RadioButton rb8=new RadioButton("Before Growth");
        rb8.setToggleGroup(group3);
        rb8.setLayoutX(225);
        rb8.setLayoutY(rb7.getLayoutY()+buttonSpace);


        Label inclusionshapeLabel=new Label("Inclusions Shape");
        inclusionshapeLabel.setLayoutX(225);
        inclusionshapeLabel.setLayoutY(rb8.getLayoutY()+buttonSpace);

        RadioButton rb9=new RadioButton("Diagonal");
        rb9.setToggleGroup(group4);
        rb9.setSelected(true);
        rb9.setLayoutX(225);
        rb9.setLayoutY(inclusionshapeLabel.getLayoutY()+buttonSpace);

        RadioButton rb10=new RadioButton("Circle");
        rb10.setToggleGroup(group4);
        rb10.setLayoutX(225);
        rb10.setLayoutY(rb9.getLayoutY()+buttonSpace);



        TextField seedText=new TextField("Random");
        seedRule=0;
        final ContextMenu c2=new ContextMenu();
        MenuItem random=new MenuItem("Random");

        MenuItem clicked=new MenuItem("On Click");

        c2.getItems().addAll(random,clicked);
        random.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                seedText.setText("Random");
                seedRule=0;

            }
        });

        clicked.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    seedText.setText("On Click");
                                    seedRule=3;
                                    numberOfFirstSeeds=0;
                                }
                            }

        );



        Label seedLabel=new Label("First Seeds Type");
        seedLabel.setLayoutX(0);

        seedLabel.setLayoutY(rb6.getLayoutY()+buttonSpace);



        seedText.setContextMenu(c2);
        seedText.setLayoutX(0);
        seedText.setLayoutY(seedLabel.getLayoutY()+buttonSpace);
        Label inclLabel=new Label("Number of inclusions");
        inclLabel.setLayoutX(0);


        inclLabel.setLayoutY(seedText.getLayoutY()+buttonSpace);
        TextField inclText=new TextField("0");
        inclText.setLayoutX(0);
        inclText.setLayoutY(inclLabel.getLayoutY()+buttonSpace);

        tf2Label.setLayoutX(0);
        tf2Label.setLayoutY(inclText.getLayoutY()+buttonSpace);

        textField2.setLayoutX(0);
        textField2.setLayoutY(tf2Label.getLayoutY()+buttonSpace);
        Label tfdualsub=new Label("Substructure type");
        tfdualsub.setLayoutX(0);
        tfdualsub.setLayoutY(textField2.getLayoutY()+buttonSpace);
        CheckBox dualsubcheck=new CheckBox();
        dualsubcheck.setText("Dual Phase");
        dualsubcheck.setLayoutX(0);
        dualsubcheck.setLayoutY(tfdualsub.getLayoutY()+buttonSpace);
        Button butsubsadded=new Button();
        butsubsadded.setText("Substructure Added");
        butsubsadded.setLayoutY(dualsubcheck.getLayoutY());
        butsubsadded.setLayoutX(225);
        Button butcolorborder=new Button();
        butcolorborder.setText("Color Borders and Clear Space");
        butcolorborder.setLayoutY(tfdualsub.getLayoutY());
        butcolorborder.setLayoutX(225);
        Label bordersizelabel=new Label("Border size");
        bordersizelabel.setLayoutY(rb10.getLayoutY());
        bordersizelabel.setLayoutX(400);
        Label enrgyLabel=new Label("Energy Distr:");
        enrgyLabel.setLayoutY(rb10.getLayoutY());
        enrgyLabel.setLayoutX(400+4*buttonSpace);
        Label enrgyedgLabel=new Label("Energy on Edges");
        enrgyedgLabel.setLayoutY(rb10.getLayoutY());
        enrgyedgLabel.setLayoutX(enrgyLabel.getLayoutX()+4*buttonSpace);
        Label enrgyinsLabel=new Label("Energy Inside");
        enrgyinsLabel.setLayoutY(rb10.getLayoutY());
        enrgyinsLabel.setLayoutX(enrgyedgLabel.getLayoutX()+4*buttonSpace);


//        temptext.setContextMenu(c1);
//        temptext.setLayoutX(0);
//        temptext.setLayoutY(neighLabel.getLayoutY()+buttonSpace);

        TextField energydistrtype=new TextField();
        energydistrtype.setText("Homogenous");
        heterogenousdistribution=false;
        energydistrtype.setLayoutY(bordersizelabel.getLayoutY()+buttonSpace);
        energydistrtype.setLayoutX(400+4*buttonSpace);
        final ContextMenu cmen=new ContextMenu();
        MenuItem homogenous=new MenuItem("Homogenous");
        MenuItem heterogenous=new MenuItem("Heterogenous");
        cmen.getItems().addAll(homogenous,heterogenous);
        homogenous.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                energydistrtype.setText("Homogenous");
                heterogenousdistribution=false;

            }
        });
        heterogenous.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                energydistrtype.setText("Heterogenous");
                heterogenousdistribution=true;

            }
        });
        energydistrtype.setContextMenu(cmen);

        TextField bordersize=new TextField();
        bordersize.setLayoutY(bordersizelabel.getLayoutY()+buttonSpace);
        bordersize.setLayoutX(400);
        bordersize.setText("1");
        TextField energyedg=new TextField();
        energyedg.setText("7");
        energyedg.setLayoutY(bordersizelabel.getLayoutY()+buttonSpace);
        energyedg.setLayoutX(energydistrtype.getLayoutX()+4*buttonSpace);
        TextField energyins=new TextField();
        energyins.setText("5");
        energyins.setLayoutY(bordersizelabel.getLayoutY()+buttonSpace);
        energyins.setLayoutX(energyedg.getLayoutX()+4*buttonSpace);
        CheckBox selectone=new CheckBox();
        selectone.setText("Select One Grain to Color Borders");
        selectone.setLayoutX(400);
        selectone.setLayoutY(bordersize.getLayoutY()+buttonSpace);
        CheckBox mc=new CheckBox();
        mc.setText("Monte Carlo");
        mc.setLayoutX(400+buttonSpace+5*buttonSpace);
        mc.setLayoutY(selectone.getLayoutY());
        CheckBox rc=new CheckBox();
        rc.setText("SRX");
        rc.setLayoutY(selectone.getLayoutY());
        rc.setLayoutX(mc.getLayoutX()+6*buttonSpace);
        CheckBox energycolor=new CheckBox();
        energycolor.setText("Energy Map");
        energycolor.setLayoutX(mc.getLayoutX()+3*buttonSpace);
        energycolor.setLayoutY(mc.getLayoutY());
        final ToggleGroup groupKryst = new ToggleGroup();
        RadioButton rbcryst=new RadioButton("REKRYSTALIZACJA");
        rbcryst.setToggleGroup(groupKryst);
        rbcryst.setSelected(false);
        rbcryst.setLayoutX(0);
        rbcryst.setLayoutY(seedText.getLayoutY()+buttonSpace);







        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                if(rb6.isSelected()) logicPeriodic=false;
                if(rb8.isSelected()) typeOfInclusion=1;
                if(rb7.isSelected()) typeOfInclusion=2;
                if(rb9.isSelected()) shapeOfInclusion=2;
                if(rb10.isSelected()) shapeOfInclusion=1;


                appHeight=Integer.parseInt(textField3.getText());
                appWidth=Integer.parseInt(textField4.getText());
                r=Integer.parseInt(textField2.getText());
                pixelSize=400/appWidth;
                canvas.setHeight(appHeight*pixelSize);
                canvas.setWidth(appWidth*pixelSize);
                numberOfFirstSeeds=Integer.parseInt(textField1.getText());
                energyJ=Integer.parseInt(jField.getText());
                numberOfInclusions=Integer.parseInt(inclText.getText());
                inclusionSize=Integer.parseInt(textField2.getText());
                probability=Integer.parseInt(textFieldprob.getText());



                drawShapes(gc,root,btnimg,btnimgimp,dualsubcheck,butsubsadded,butcolorborder,bordersize,selectone,mc,energyedg,energyins,btndist,energycolor,nField,rc,numberofnuclField);

            }
        });




        //end
        root.getChildren().add(canvas);
        root.getChildren().addAll(btn,btnimg,btndist,numberofnucleons,numberofnuclField);
        root.getChildren().addAll(neighLabel,jField,jlabel,Nlabel,nField,nucletionmodellabel,nuclmodelfield,nucwherelabel,nuclwherefield,rc);
        root.getChildren().addAll(periodicLabel, rb6, inclusionsLabel, rb7,rb8,rb9,rb10,inclusionshapeLabel,enrgyLabel,energydistrtype,energyedg,energyins,enrgyedgLabel,enrgyinsLabel,energycolor);
        root.getChildren().addAll(seedLabel, inclLabel);

        root.getChildren().addAll(tf1Label,tfLabelprob,tf2Label,dualsubcheck,butsubsadded,selectone,mc,bordersize,bordersizelabel,butcolorborder,tfdualsub,tf3Label,tf4Label,textField1,textField2,textField3,textFieldprob,textField4,temptext,seedText,inclText);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();


    }

    private void drawShapes(GraphicsContext gc, Group root, Button btnimg,Button btnimgimp,CheckBox dualphase, Button DualStart, Button ColorBorders, TextField bordersizefield, CheckBox selectonegrain, CheckBox mc, TextField energyonedge, TextField energyinside, Button btndist, CheckBox energycolor, TextField mcstepsfield, CheckBox rc, TextField nuclnumb) {
        Logic logic = new Logic(appWidth, appHeight, numberOfFirstSeeds, numberOfInclusions,typeOfInclusion,inclusionSize,shapeOfInclusion,probability,energyJ);
        logic.setNeigh(logicNeighbourhood);
        logic.setPeriodic(logicPeriodic);

        logic.setSeedRule(seedRule);
        logic.setR(r);
        logic.mc=mc.isSelected();
        if (logic.mc)
        {
            logic.mcsteps=Integer.parseInt(mcstepsfield.getText());
        }

        logic.start();


//new


        //klikanie to ziarno start
        root.setOnMouseClicked(
                new EventHandler<MouseEvent>()
                {
                    public void handle(MouseEvent e)
                    {
                        int x=(int)(e.getX());
                        int y=(int)(e.getY());
//System.out.println((x-600)/pixelSize+" "+y/pixelSize);
                       if (!logic.finished()&&!logic.mc) logic.newSeed((x-600)/pixelSize,y/pixelSize);
                       else {
                           if (!selectonegrain.isSelected()) {
                               logic.setfinished=true;
                               System.out.println("SEEDID(" + ((y / pixelSize) + 1) + "," + (((x - 600) / pixelSize) + 1) + "): " + logic.map[(y / pixelSize) + 1][((x - 600) / pixelSize) + 1].seedid + " KOLOR: " + logic.map[y / pixelSize + 1][((x - 600) / pixelSize) + 1].getColor() + "BORDER: " + logic.map[y / pixelSize + 1][(x - 600) / pixelSize + 1].border);
                               logic.substructure(logic.map[(y / pixelSize) + 1][((x - 600) / pixelSize) + 1].seedid, dualphase.isSelected());
                           }
                           else
                           {
                               logic.select(!selectonegrain.isSelected(),logic.map[(y / pixelSize) + 1][((x - 600) / pixelSize) + 1].seedid);
                           }

                       }
                    }
                });
        //clicked ziarno koniec

        Timeline gameLoop = new Timeline();
        gameLoop.setCycleCount( Timeline.INDEFINITE );

        final long timeStart = System.currentTimeMillis();

        KeyFrame kf = new KeyFrame(
                Duration.seconds(0.15),                // 60 FPS
                new EventHandler<ActionEvent>()
                {
                    public void handle(ActionEvent ae) {
                        //if (logic.emptyFields() > 0) {

//System.out.println("Początek rysowania");
                        logic.energycolor();
                        for (int i = 0; i < appHeight; i++) {

                            for (int j = 0; j < appWidth; j++) {
                                    if (energycolor.isSelected())
                                    {
                                        gc.setFill(logic.energycolormap.get(logic.map[j + 1][i + 1].energy));
                                    }
                                    else {
                                        gc.setFill(logic.map[j + 1][i + 1].getColor());
                                    }
                                    gc.fillRect(i * pixelSize, j * pixelSize, pixelSize, pixelSize);

                            }

                        }






                            //System.out.println("Koniec rysowania");

                        logic.setNeigh(logicNeighbourhood);


                    logic.nextStep();
                    if (logic.mcsteps==0) {
                        mc.setSelected(false);
                        logic.mc = false;
                        rc.setSelected(false);
                        logic.recr=false;
                    }
                    }
                });



        gameLoop.getKeyFrames().add( kf );
        gameLoop.play();

        //koniec new

        btnimg.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                if (logic.finished())
                {
                    try {
                        logic.createImage(appWidth,appHeight);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }





            }
        });
        btndist.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                if (heterogenousdistribution)
                {
                    logic.distributionofenergy(Integer.parseInt(energyinside.getText()),Integer.parseInt(energyonedge.getText()));
                }
                else
                {
                    logic.distributionofenergy(Integer.parseInt(energyinside.getText()),Integer.parseInt(energyinside.getText()));
                }





            }
        });
        mc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


              if (mc.isSelected())
              {
                  logic.mc=true;
                  logic.mcsteps=Integer.parseInt(mcstepsfield.getText());
              }
              else {
                  logic.mc = false;
                  logic.mcsteps=0;
              }




            }
        });
        rc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                if (rc.isSelected())
                {
                    mc.setSelected(false);
                    logic.mc=false;
                    logic.recr=true;
                    logic.mcsteps=Integer.parseInt(mcstepsfield.getText());
                    logic.nucloc=nucloc;
                    logic.nucappmod=nucmod;
                    logic.numberofnucleons=Integer.parseInt(nuclnumb.getText());


                }
                else {
                    logic.recr = false;
                    logic.mcsteps=0;
                }




            }
        });
        btnimgimp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                try {

                    logic.ReadImage(appWidth,appHeight);


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
        DualStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {



                    logic.clearallexceptsubstructure();
                    logic.secondgrowth=true;
                    logic.start();






            }
        });
        ColorBorders.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {


                if (logic.finished()||logic.secondgrowth)
                {
                    if (selectonegrain.isSelected()) {
                        logic.settingBordersize(Integer.parseInt(bordersizefield.getText()), selectonegrain.isSelected());
                        logic.clearspace();
                    }
                    else
                    {
                        logic.select(true,0);
                        logic.settingBordersize(Integer.parseInt(bordersizefield.getText()), selectonegrain.isSelected());
                        logic.clearspace();
                    }
                   //colorborder=true;
                }





            }
        });

    }
}
