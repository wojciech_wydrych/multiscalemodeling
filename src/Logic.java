import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Logic {
    public Cell[][] map,newmap;

    private int width,height;
    private int firstGeneration;
    private int inclusionnum;
    private boolean postinclumade;
    private int inctype;
    private int neigh=0;
    private int incshape;
    private int sizeOfInclusion;
    private int probability;
    public Map<Integer,Color> seedcolormap;
    public Map<Integer,Color> energycolormap;
    public Color substructcolor;
    public boolean importing;
    public boolean mc;
    public int en_grain_bound=1;
    public boolean setfinished=false;
    public int mcsteps=0;
    public boolean recr=false;
    public int numberofnucleons=10;
    public int nucappmod=1;
    public int nucloc=1;
    private static int nuclid=-100;

    boolean choice=false;
    boolean periodic=true;
    int seedRule=0;
    int r;
    boolean secondgrowth=false;
    public Logic(int width, int height, int firstGeneration, int inclusionnum, int inctype,int inclusionsize, int incshape,int probability, int energyJ) {
        this.width = (width+2);
        this.height = (height+2);
        this.firstGeneration = firstGeneration;
        this.inclusionnum=inclusionnum;
        this.inctype=inctype;
        this.sizeOfInclusion=inclusionsize;
        this.incshape=incshape;
        this.probability=probability;
        seedcolormap=new HashMap<Integer, Color>();
        this.importing=false;
        this.en_grain_bound=energyJ;



        map=new Cell[height+2][width+2];
        newmap=new Cell[height+2][width+2];
        energycolormap=new HashMap<Integer,Color>();


    }


    public void updateMap(){

        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){


                map[i][j].setState(newmap[i][j].isState());
                map[i][j].setColor(newmap[i][j].getColor());
                map[i][j].setdstate(newmap[i][j].seedid);
                map[i][j].border=newmap[i][j].border;
                map[i][j].energy=newmap[i][j].energy;
                map[i][j].recr=newmap[i][j].recr;

            }
        }
        //finished();


    }
    public void newSeed(int x,int y){
        map[y + 1][x + 1].setState(true);
        Random rng=new Random();

        int a,b,c;
        a=rng.nextInt(255);
        b=rng.nextInt(255);
        c=rng.nextInt(255);
        map[y + 1][x + 1].setColor(Color.rgb(a, b, c));

    }
    public void start() {
        if (!this.secondgrowth) {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    map[i][j] = new Cell();
                    newmap[i][j] = new Cell();
                }
            }
        }
postinclumade=false;
        if(seedRule==0) {
            int x, y;
            Random rng = new Random();
            for (int i = 0; i < inclusionnum && inctype == 1; i++) {
                do {
                    x = rng.nextInt(width - 2);
                    y = rng.nextInt(height - 2);
                } while (map[y + 1][x + 1].substructure || map[y + 1][x + 1].inclusion);
                map[y + 1][x + 1].setState(true);
                map[y + 1][x + 1].setdstate(-1);
                map[y + 1][x + 1].inclusion = true;
                newmap[y + 1][x + 1].setState(true);
                map[y + 1][x + 1].setColor(Color.rgb(255, 255, 255));
                newmap[y + 1][x + 1].setColor(map[y + 1][x + 1].getColor());
                newmap[y + 1][x + 1].setdstate(-1);
                newmap[y + 1][x + 1].inclusion = true;
                x = x + 1;
                y = y + 1;


                for (int j = 1; j < height - 1; j++) {
                    for (int q = 1; q < width - 1; q++) {

                        if (incshape == 1) {

                            if (sqrt((j - y) * (j - y) + (q - x) * (q - x)) <= sizeOfInclusion) {
                                map[j][q].setState(true);
                                map[j][q].setdstate(-1);
                                map[j][q].inclusion = true;
                                map[j][q].setColor(Color.rgb(255, 255, 255));
                                newmap[j][q].setColor(Color.rgb(255, 255, 255));
                                newmap[j][q].setState(true);
                                newmap[j][q].setdstate(-1);
                                newmap[j][q].inclusion = true;
                            }
                        }

                        if (incshape == 2) {
                            boolean ytrue = false, xtrue = false;
                            double a = 0.5 * sizeOfInclusion * sqrt(2);

                            if (j < y) {
                                if (y - j <= (0.5 * a)) {
                                    ytrue = true;
                                }
                            } else {
                                if (j - y <= (0.5 * a)) {
                                    ytrue = true;
                                }
                            }

                            if (q < x) {
                                if (x - q <= (0.5 * a)) {
                                    xtrue = true;
                                }
                            } else {
                                if (q - x <= (0.5 * a)) {
                                    xtrue = true;
                                }
                            }


                            if (xtrue && ytrue) {
                                map[j][q].setState(true);
                                map[j][q].setdstate(-1);
                                map[j][q].inclusion = true;
                                map[j][q].setColor(Color.rgb(255, 255, 255));
                                newmap[j][q].setColor(Color.rgb(255, 255, 255));
                                newmap[j][q].setState(true);
                                newmap[j][q].setdstate(-1);
                                newmap[j][q].inclusion = true;
                            }
                        }
                    }
                }


            }
            if (!mc)
            {
                for (int i = 0; i < firstGeneration; i++) {

                    do {
                        x = rng.nextInt(width - 2);
                        y = rng.nextInt(height - 2);
                    }
                    while (map[y + 1][x + 1].inclusion || map[y + 1][x + 1].isState() || map[y + 1][x + 1].substructure);
                    map[y + 1][x + 1].setState(true);
                    map[y + 1][x + 1].setdstate(i);
                    newmap[y + 1][x + 1].setState(true);
                    do {
                        map[y + 1][x + 1].setColor(Color.rgb(rng.nextInt(1), rng.nextInt(255), rng.nextInt(255)));
                    }
                    while (seedcolormap.containsValue(map[y + 1][x + 1].getColor()) || map[y + 1][x + 1].getColor() == Color.BLACK || map[y + 1][x + 1].getColor() == Color.WHITE);
                    seedcolormap.put(i, map[y + 1][x + 1].getColor());
                    newmap[y + 1][x + 1].setColor(map[y + 1][x + 1].getColor());
                    newmap[y + 1][x + 1].setdstate(i);
                }
        } else
            {
                for (int i = 0; i < firstGeneration; i++)
                {
                    Color tempcol;
                    do {
                       tempcol=Color.rgb(rng.nextInt(1), rng.nextInt(255), rng.nextInt(255));
                    }
                    while (seedcolormap.containsValue(tempcol) || tempcol == Color.BLACK || tempcol == Color.WHITE);
                    seedcolormap.put(i, tempcol);
                }
                int temp;
                for (int i=0;i<(width-2);i++)
                {
                    for (int j=0;j<(height-2);j++)
                    {
                        if (!map[j+1][i+1].substructure&&!map[j+1][i+1].inclusion) {
                            temp = rng.nextInt(firstGeneration);
                            map[j + 1][i + 1].setState(true);
                            map[j + 1][i + 1].setdstate(temp);
                            newmap[j + 1][i + 1].setState(true);
                            newmap[j + 1][i + 1].setdstate(temp);
                            map[j + 1][i + 1].setColor(seedcolormap.get(temp));
                            newmap[j + 1][i + 1].setColor(seedcolormap.get(temp));
                        }
                    }
                }

            }

        }


    }

    public boolean finished()
    {
        int count=0;
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                if (map[i][j].isState()==true) count++;

            }
        }
       // System.out.println("Size: "+((width-2)*(height-2))+" Zliczone "+count);
        if (count==((width-2)*(height-2))||setfinished) return true; else return false;
    }


//    private void smoora(int i, int j){
//        if(map[i][j].isState()&&map[i][j].inclusion==false) {
//            if(!map[i-1][j-1].isState()){
//                newmap[i-1][j-1].setColor(map[i][j].getColor());
//                newmap[i-1][j-1].setState(true);
//                newmap[i-1][j-1].seedid=map[i][j].seedid;
//            }
//            if(!map[i-1][j].isState()){
//                newmap[i-1][j].setColor(map[i][j].getColor());
//                newmap[i-1][j].setState(true);
//                newmap[i-1][j].seedid=map[i][j].seedid;
//            }
//            if(!map[i-1][j+1].isState()){
//                newmap[i-1][j+1].setColor(map[i][j].getColor());
//                newmap[i-1][j+1].setState(true);
//                newmap[i-1][j+1].seedid=map[i][j].seedid;
//            }
//            if(!map[i][j-1].isState()){
//                newmap[i][j-1].setColor(map[i][j].getColor());
//                newmap[i][j-1].setState(true);
//                newmap[i][j-1].seedid=map[i][j].seedid;
//            }
//            if(!map[i][j+1].isState()){
//                newmap[i][j+1].setColor(map[i][j].getColor());
//                newmap[i][j+1].setState(true);
//                newmap[i][j+1].seedid=map[i][j].seedid;
//            }
//            if(!map[i+1][j-1].isState()){
//                newmap[i+1][j-1].setColor(map[i][j].getColor());
//                newmap[i+1][j-1].setState(true);
//                newmap[i+1][j-1].seedid=map[i][j].seedid;
//            }
//            if(!map[i+1][j].isState()){
//                newmap[i+1][j].setColor(map[i][j].getColor());
//                newmap[i+1][j].setState(true);
//                newmap[i+1][j].seedid=map[i][j].seedid;
//            }
//            if(!map[i+1][j+1].isState()){
//                newmap[i+1][j+1].setColor(map[i][j].getColor());
//                newmap[i+1][j+1].setState(true);
//                newmap[i+1][j+1].seedid=map[i][j].seedid;
//            }
//        }
//    }
    private void smoora(int i, int j)
    {
        if(!map[i][j].isState())
        {
            Map<String,Integer> neighcounter=new HashMap<String, Integer>();
            Map<String,Color> neighcolors=new HashMap<String, Color>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){
                String tempseed=Integer.toString(map[i-1][j-1].seedid);
                Color tempcolor=map[i-1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                String tempseed=Integer.toString(map[i-1][j].seedid);
                Color tempcolor=map[i-1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                String tempseed=Integer.toString(map[i-1][j+1].seedid);
                Color tempcolor=map[i-1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                String tempseed=Integer.toString(map[i][j-1].seedid);
                Color tempcolor=map[i][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
            String tempseed=Integer.toString(map[i][j+1].seedid);
            Color tempcolor=map[i][j+1].getColor();
            if (neighcounter.containsKey(tempseed))
            {
                neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
            }
            else
            {
                neighcounter.put(tempseed,1);
                neighcolors.put(tempseed,tempcolor);
            }
        }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                String tempseed=Integer.toString(map[i+1][j-1].seedid);
                Color tempcolor=map[i+1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                String tempseed=Integer.toString(map[i+1][j].seedid);
                Color tempcolor=map[i+1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                String tempseed=Integer.toString(map[i+1][j+1].seedid);
                Color tempcolor=map[i+1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }

            if(!neighcounter.isEmpty())
            {
                int maxValueInMap=(Collections.max(neighcounter.values()));  // This will return max value in the Hashmap
                for (Map.Entry<String, Integer> entry : neighcounter.entrySet()) {  // Itrate through hashmap
                    if (entry.getValue()==maxValueInMap) {
                        newmap[i][j].setColor(neighcolors.get(entry.getKey()));
                        newmap[i][j].setState(true);
                        newmap[i][j].seedid=Integer.parseInt(entry.getKey());
                    }
                }

            }



        }


    }
    private int delta(int i, int j)
    {
        int delta_cron=0;
        if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

            if (map[i-1][j-1].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
            if (map[i-1][j].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
            if (map[i-1][j+1].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
            if (map[i][j-1].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
            if (map[i][j+1].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
            if (map[i+1][j-1].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
            if (map[i+1][j].seedid!=map[i][j].seedid) delta_cron++;
        }
        if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
            if (map[i+1][j+1].seedid!=map[i][j].seedid) delta_cron++;
        }

        return delta_cron;
}
public void distributionofenergy(int inside, int onedges)
{
    settingBorder();

    for (int i=0;i<(width-2);i++)
    {
        for (int j=0;j<(height-2);j++)
        {
         //   if (!map[j+1][i+1].substructure&&!map[j+1][i+1].inclusion) {

                if (map[j+1][i+1].border)
                {
                    map[j+1][i+1].energy+=onedges;
                    newmap[j+1][i+1].energy+=onedges;
                } else {
                    map[j+1][i+1].energy+=inside;
                    newmap[j+1][i+1].energy+=inside;
                }
     //       }
        }
    }
    energycolor();
}

public void energycolor()
{
    for (int i=0;i<(width-2);i++)
    {
        for (int j=0;j<(height-2);j++)
        {
            //   if (!map[j+1][i+1].substructure&&!map[j+1][i+1].inclusion) {

                if (!energycolormap.containsKey(map[j+1][i+1].energy))
                {
                    Random rng=new Random();

                    int a,b,c;
                    a=rng.nextInt(255);
                    b=rng.nextInt(255);
                    c=rng.nextInt(255);
                    if (map[j+1][i+1].energy==0) energycolormap.put(map[j+1][i+1].energy,Color.rgb(255,0,0));

                    else energycolormap.put(map[j+1][i+1].energy,Color.rgb(0,b,c));
                }

            //       }
        }
    }
}
    private void smooramc(int i, int j)
    {
       // System.out.println("beforetif");
        if(map[i][j].isState())
        {

            int delta_cron=0;
            double en_bef, en_aft;
            int seedidprev;
            int neig_id_counter=0;
            Map<Integer,Integer> neigh=new HashMap<Integer, Integer>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                if (map[i-1][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i-1][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j-1].seedid);
                    }
                }
            }
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                if (map[i-1][j].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i-1][j].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j].seedid);
                    }
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                if (map[i-1][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i-1][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j+1].seedid);
                    }
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                if (map[i][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i][j-1].seedid);
                    }
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                if (map[i][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i][j+1].seedid);
                    }
                }
            }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                if (map[i+1][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i+1][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j-1].seedid);
                    }
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                if (map[i+1][j].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i+1][j].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j].seedid);
                    }
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                if (map[i+1][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (!neigh.containsValue(map[i+1][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j+1].seedid);
                    }
                }
            }

            if (neig_id_counter>0)
            {
                Random rng=new Random();
                en_bef=delta_cron*en_grain_bound;
                int x;
                if (neig_id_counter!=1) x=rng.nextInt(neig_id_counter-1);
                else x=0;
                seedidprev=map[i][j].seedid;
                map[i][j].seedid=neigh.get(x+1);
                newmap[i][j].seedid=neigh.get(x+1);
                map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                delta_cron=0;


                if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                    if (map[i-1][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                    if (map[i-1][j].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                    if (map[i-1][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                    if (map[i][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                    if (map[i][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                    if (map[i+1][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                    if (map[i+1][j].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                    if (map[i+1][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }

                en_aft=delta_cron*en_grain_bound;
                if (en_aft-en_bef>0)
                {
                    map[i][j].seedid=seedidprev;
                    newmap[i][j].seedid=seedidprev;
                    map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                    newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                  //  System.out.println("Nie zmieniamy:(");
                }
                else
                {
                   // System.out.println("Zmieniamy:)");
                }

            }


        }


    }
    private void smooramcrec(int i, int j)
    {
        // System.out.println("beforetif");
        if(!map[i][j].recr)
        {

            int delta_cron=0;
            double en_bef, en_aft;
            int recr_count=0;
            int seedidprev;
            int neig_id_counter=0;
            Map<Integer,Integer> neigh=new HashMap<Integer, Integer>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                if (map[i-1][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i-1][j-1].recr&&!neigh.containsValue(map[i-1][j-1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i-1][j-1].seedid);
                    }
                }
            }
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                if (map[i-1][j].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i-1][j].recr&&!neigh.containsValue(map[i-1][j].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i-1][j].seedid);
                    }
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                if (map[i-1][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i-1][j+1].recr&&!neigh.containsValue(map[i-1][j+1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i-1][j+1].seedid);
                    }
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                if (map[i][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i][j-1].recr&&!neigh.containsValue(map[i][j-1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i][j-1].seedid);
                    }
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                if (map[i][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i][j+1].recr&&!neigh.containsValue(map[i][j+1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i][j+1].seedid);
                    }
                }
            }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                if (map[i+1][j-1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i+1][j-1].recr&&!neigh.containsValue(map[i+1][j-1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i+1][j-1].seedid);
                    }
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                if (map[i+1][j].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i+1][j].recr&&!neigh.containsValue(map[i+1][j].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i+1][j].seedid);
                    }
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                if (map[i+1][j+1].seedid!=map[i][j].seedid)
                {
                    delta_cron++;
                    if (map[i+1][j+1].recr&&!neigh.containsValue(map[i+1][j+1].seedid))
                    {
                        recr_count++;
                        neigh.put(recr_count,map[i+1][j+1].seedid);
                    }
                }
            }

            if (recr_count>0)
            {
                Random rng=new Random();
                en_bef=delta_cron*en_grain_bound+map[i][j].energy;
                int x;
                if (recr_count!=1) x=rng.nextInt(recr_count-1);
                else x=0;
                seedidprev=map[i][j].seedid;
                map[i][j].seedid=neigh.get(x+1);
                newmap[i][j].seedid=neigh.get(x+1);
                map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                map[i][j].recr=true;
                newmap[i][j].recr=true;
                delta_cron=0;


                if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                    if (map[i-1][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                    if (map[i-1][j].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                    if (map[i-1][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                    if (map[i][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                    if (map[i][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                    if (map[i+1][j-1].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                    if (map[i+1][j].seedid!=map[i][j].seedid) delta_cron++;
                }
                if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                    if (map[i+1][j+1].seedid!=map[i][j].seedid) delta_cron++;
                }

                en_aft=delta_cron*en_grain_bound;
                if (en_aft-en_bef>0)
                {
                    map[i][j].seedid=seedidprev;
                    newmap[i][j].seedid=seedidprev;
                    map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                    newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                    map[i][j].recr=false;
                    newmap[i][j].recr=false;
                //    System.out.println("Nie zmieniamy rekr:(");
                }
                else
                {
                    map[i][j].energy=0;
                    newmap[i][j].energy=0;
                    // System.out.println("Zmieniamy:)");
                }

            }


        }


    }
    private void smooramc2(int i, int j)
    {
        // System.out.println("beforetif");
        if(map[i][j].isState())
        {

            int suma=0;
            double en_bef, en_aft;
            int seedidprev;
            int neig_id_counter=0;
            Map<Integer,Integer> neigh=new HashMap<Integer, Integer>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                if (map[i-1][j-1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i-1,j-1));
                    if (!neigh.containsValue(map[i-1][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j-1].seedid);
                    }
                }
            }
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                if (map[i-1][j].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i-1,j));
                    if (!neigh.containsValue(map[i-1][j].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j].seedid);
                    }
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                if (map[i-1][j+1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i-1,j+1));
                    if (!neigh.containsValue(map[i-1][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i-1][j+1].seedid);
                    }
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                if (map[i][j-1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i,j-1));
                    if (!neigh.containsValue(map[i][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i][j-1].seedid);
                    }
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                if (map[i][j+1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i,j+1));
                    if (!neigh.containsValue(map[i][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i][j+1].seedid);
                    }
                }
            }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                if (map[i+1][j-1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i+1,j-1));
                    if (!neigh.containsValue(map[i+1][j-1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j-1].seedid);
                    }
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                if (map[i+1][j].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i+1,j));
                    if (!neigh.containsValue(map[i+1][j].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j].seedid);
                    }
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                if (map[i+1][j+1].seedid!=map[i][j].seedid)
                {
                    suma+=(1-delta(i+1,j+1));
                    if (!neigh.containsValue(map[i+1][j+1].seedid))
                    {
                        neig_id_counter++;
                        neigh.put(neig_id_counter,map[i+1][j+1].seedid);
                    }
                }
            }

            if (neig_id_counter>0)
            {
                Random rng=new Random();
                en_bef=suma*en_grain_bound;
                int x;
                if (neig_id_counter!=1) x=rng.nextInt(neig_id_counter-1);
                else x=0;
                seedidprev=map[i][j].seedid;
                map[i][j].seedid=neigh.get(x+1);
                newmap[i][j].seedid=neigh.get(x+1);
                map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                suma=0;


                if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){

                    if (map[i-1][j-1].seedid!=map[i][j].seedid) suma+=(1-delta(i-1,j-1));
                }
                if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                    if (map[i-1][j].seedid!=map[i][j].seedid) suma+=(1-delta(i-1,j));
                }
                if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                    if (map[i-1][j+1].seedid!=map[i][j].seedid) suma+=(1-delta(i-1,j+1));
                }
                if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                    if (map[i][j-1].seedid!=map[i][j].seedid) suma+=(1-delta(i,j-1));
                }
                if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                    if (map[i][j+1].seedid!=map[i][j].seedid) suma+=(1-delta(i,j+1));
                }
                if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                    if (map[i+1][j-1].seedid!=map[i][j].seedid) suma+=(1-delta(i+1,j-1));
                }
                if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                    if (map[i+1][j].seedid!=map[i][j].seedid) suma+=(1-delta(i+1,j));
                }
                if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                    if (map[i+1][j+1].seedid!=map[i][j].seedid) suma+=(1-delta(i+1,j+1));
                }

                en_aft=suma*en_grain_bound;
                if (en_aft-en_bef>0)
                {
                    map[i][j].seedid=seedidprev;
                    newmap[i][j].seedid=seedidprev;
                    map[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                    newmap[i][j].setColor(seedcolormap.get(map[i][j].seedid));
                    //  System.out.println("Nie zmieniamy:(");
                }
                else
                {
                    // System.out.println("Zmieniamy:)");
                }

            }


        }


    }
    private void smoora2(int i, int j)
    {
        if(!map[i][j].isState())
        {
            Map<String,Integer> neighcounter=new HashMap<String, Integer>();
            Map<String,Color> neighcolors=new HashMap<String, Color>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){
                String tempseed=Integer.toString(map[i-1][j-1].seedid);
                Color tempcolor=map[i-1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                String tempseed=Integer.toString(map[i-1][j].seedid);
                Color tempcolor=map[i-1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                String tempseed=Integer.toString(map[i-1][j+1].seedid);
                Color tempcolor=map[i-1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                String tempseed=Integer.toString(map[i][j-1].seedid);
                Color tempcolor=map[i][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                String tempseed=Integer.toString(map[i][j+1].seedid);
                Color tempcolor=map[i][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                String tempseed=Integer.toString(map[i+1][j-1].seedid);
                Color tempcolor=map[i+1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                String tempseed=Integer.toString(map[i+1][j].seedid);
                Color tempcolor=map[i+1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                String tempseed=Integer.toString(map[i+1][j+1].seedid);
                Color tempcolor=map[i+1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }

            if(!neighcounter.isEmpty())
            {
                int maxValueInMap=(Collections.max(neighcounter.values()));  // This will return max value in the Hashmap
                for (Map.Entry<String, Integer> entry : neighcounter.entrySet()) {  // Itrate through hashmap
                    if (entry.getValue()==maxValueInMap&&maxValueInMap>=5) {
                        newmap[i][j].setColor(neighcolors.get(entry.getKey()));
                        newmap[i][j].setState(true);
                        newmap[i][j].seedid=Integer.parseInt(entry.getKey());
                    }
                    else
                    {
                        smoorarule2(i,j);
//                        System.out.println("Reguła 2");
                    }
                }

            }



        }


    }
    private void smoorarule2(int i, int j)
    {
        if(!map[i][j].isState())
        {
            Map<String,Integer> neighcounter=new HashMap<String, Integer>();
            Map<String,Color> neighcolors=new HashMap<String, Color>();
            if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
                String tempseed=Integer.toString(map[i-1][j].seedid);
                Color tempcolor=map[i-1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
                String tempseed=Integer.toString(map[i][j-1].seedid);
                Color tempcolor=map[i][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
                String tempseed=Integer.toString(map[i][j+1].seedid);
                Color tempcolor=map[i][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
                String tempseed=Integer.toString(map[i+1][j].seedid);
                Color tempcolor=map[i+1][j].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }

            if(!neighcounter.isEmpty())
            {
                int maxValueInMap=(Collections.max(neighcounter.values()));  // This will return max value in the Hashmap
                for (Map.Entry<String, Integer> entry : neighcounter.entrySet()) {  // Itrate through hashmap
                    if (entry.getValue()==maxValueInMap&&maxValueInMap>=3) {
                        newmap[i][j].setColor(neighcolors.get(entry.getKey()));
                        newmap[i][j].setState(true);
                        newmap[i][j].seedid=Integer.parseInt(entry.getKey());
                    }
                    else
                    {
                        smoorarule3(i,j);
//                        System.out.println("Reguła3");
                    }
                }

            }
            else smoorarule3(i,j);



        }


    }

    private void smoorarule3(int i, int j)
    {
        if(!map[i][j].isState())
        {
            Map<String,Integer> neighcounter=new HashMap<String, Integer>();
            Map<String,Color> neighcolors=new HashMap<String, Color>();
            if(map[i-1][j-1].isState()&&!map[i-1][j-1].inclusion&&!map[i-1][j-1].substructure){
                String tempseed=Integer.toString(map[i-1][j-1].seedid);
                Color tempcolor=map[i-1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i-1][j+1].isState()&&!map[i-1][j+1].inclusion&&!map[i-1][j+1].substructure){
                String tempseed=Integer.toString(map[i-1][j+1].seedid);
                Color tempcolor=map[i-1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j+1].isState()&&!map[i+1][j+1].inclusion&&!map[i+1][j+1].substructure){
                String tempseed=Integer.toString(map[i+1][j+1].seedid);
                Color tempcolor=map[i+1][j+1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }
            if(map[i+1][j-1].isState()&&!map[i+1][j-1].inclusion&&!map[i+1][j-1].substructure){
                String tempseed=Integer.toString(map[i+1][j-1].seedid);
                Color tempcolor=map[i+1][j-1].getColor();
                if (neighcounter.containsKey(tempseed))
                {
                    neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
                }
                else
                {
                    neighcounter.put(tempseed,1);
                    neighcolors.put(tempseed,tempcolor);
                }
            }

            if(!neighcounter.isEmpty())
            {
                int maxValueInMap=(Collections.max(neighcounter.values()));  // This will return max value in the Hashmap
                for (Map.Entry<String, Integer> entry : neighcounter.entrySet()) {  // Itrate through hashmap
                    if (entry.getValue()==maxValueInMap&&maxValueInMap>=3) {
                        newmap[i][j].setColor(neighcolors.get(entry.getKey()));
                        newmap[i][j].setState(true);
                        newmap[i][j].seedid=Integer.parseInt(entry.getKey());
                    }
                    else
                    {
                        Random rng=new Random();
                        int randomnumber=rng.nextInt(99)+1;
                        if (randomnumber<probability)
                        {
//                            System.out.println("PROB:"+randomnumber);
                            smoora(i,j);

                        }

                    }
                }

            }




        }


    }

//    private void sneumanna(int i, int j){
//        if(map[i][j].isState()&&map[i][j].inclusion==false) {
//            if(!map[i-1][j].isState()){
//                newmap[i-1][j].setColor(map[i][j].getColor());
//                newmap[i-1][j].setState(true);
//                newmap[i-1][j].seedid=map[i][j].seedid;
//            }
//            if(!map[i][j-1].isState()){
//                newmap[i][j-1].setColor(map[i][j].getColor());
//                newmap[i][j-1].setState(true);
//                newmap[i][j-1].seedid=map[i][j].seedid;
//            }
//            if(!map[i][j+1].isState()){
//                newmap[i][j+1].setColor(map[i][j].getColor());
//                newmap[i][j+1].setState(true);
//                newmap[i][j+1].seedid=map[i][j].seedid;
//            }
//            if(!map[i+1][j].isState()){
//                newmap[i+1][j].setColor(map[i][j].getColor());
//                newmap[i+1][j].setState(true);
//                newmap[i+1][j].seedid=map[i][j].seedid;
//            }
//        }
//    }

private void sneumanna(int i, int j)
{
    if(!map[i][j].isState())
    {
Map<String,Integer> neighcounter=new HashMap<String, Integer>();
Map<String,Color> neighcolors=new HashMap<String, Color>();
        if(map[i-1][j].isState()&&!map[i-1][j].inclusion&&!map[i-1][j].substructure){
            String tempseed=Integer.toString(map[i-1][j].seedid);
            Color tempcolor=map[i-1][j].getColor();
           if (neighcounter.containsKey(tempseed))
            {
                neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
            }
            else
           {
               neighcounter.put(tempseed,1);
               neighcolors.put(tempseed,tempcolor);
           }
        }
        if(map[i][j-1].isState()&&!map[i][j-1].inclusion&&!map[i][j-1].substructure){
            String tempseed=Integer.toString(map[i][j-1].seedid);
            Color tempcolor=map[i][j-1].getColor();
            if (neighcounter.containsKey(tempseed))
            {
                neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
            }
            else
            {
                neighcounter.put(tempseed,1);
                neighcolors.put(tempseed,tempcolor);
            }
        }
        if(map[i][j+1].isState()&&!map[i][j+1].inclusion&&!map[i][j+1].substructure){
            String tempseed=Integer.toString(map[i][j+1].seedid);
            Color tempcolor=map[i][j+1].getColor();
            if (neighcounter.containsKey(tempseed))
            {
                neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
            }
            else
            {
                neighcounter.put(tempseed,1);
                neighcolors.put(tempseed,tempcolor);
            }
        }
        if(map[i+1][j].isState()&&!map[i+1][j].inclusion&&!map[i+1][j].substructure){
            String tempseed=Integer.toString(map[i+1][j].seedid);
            Color tempcolor=map[i+1][j].getColor();
            if (neighcounter.containsKey(tempseed))
            {
                neighcounter.put(tempseed,neighcounter.get(tempseed)+1);
            }
            else
            {
                neighcounter.put(tempseed,1);
                neighcolors.put(tempseed,tempcolor);
            }
        }

        if(!neighcounter.isEmpty())
        {
            int maxValueInMap=(Collections.max(neighcounter.values()));  // This will return max value in the Hashmap
            for (Map.Entry<String, Integer> entry : neighcounter.entrySet()) {  // Itrate through hashmap
                if (entry.getValue()==maxValueInMap) {
                    newmap[i][j].setColor(neighcolors.get(entry.getKey()));
                    newmap[i][j].setState(true);
                    newmap[i][j].seedid=Integer.parseInt(entry.getKey());
                }
            }

        }



    }


}








    private void makePeriodic(){
        for(int i=1;i<(width-1);i++) {
            map[0][i] = map[height-2][i];
            map[height-1][i]=map[1][i];

        }
        for(int i=1;i<(height-1);i++){
            map[i][0]=map[i][width-2];
            map[i][width-1]=map[i][1];
        }
    }

    public void nextStep(){

        if(periodic)
            makePeriodic();
        Random rng=new Random();
        if (!mc) {
                for (int i = 1; i < height - 1; i++) {
                    for (int j = 1; j < width - 1; j++) {
                        if (neigh == 0) smoora(i, j);
                        else if (neigh == 1) sneumanna(i, j);
                        else if (neigh == 2) smoora2(i, j);


            //                }

                    }
                }
            } else
            {
                if (mcsteps>0) {
                    Map<Integer, int[]> allspace = new HashMap<Integer, int[]>();
                    int c = -1;
                    for (int i = 1; i < height - 1; i++) {
                        for (int j = 1; j < width - 1; j++) {
                            c++;
                            int[] t = new int[2];
                            t[0] = i;
                            t[1] = j;
                            allspace.put(c, t);
                        }
                    }
                    int max = c;
                    do {

                        int temp = rng.nextInt(c + 1);
                        // System.out.println("before get "+c);
                        int t[] = allspace.get(temp);

                        //System.out.println("c: " + c + " " + t[0]);


                        // System.out.println(t[1]);
                        smooramc(t[0], t[1]);
                        int tmax[] = allspace.get(c);
                        allspace.put(temp, tmax);
                        allspace.remove(c);

                        c--;
                    } while (!allspace.isEmpty());
                    mcsteps--;
                }
            }
         //   System.out.println("Logic: "+recr);
            if (recr)
            {
                //System.out.println("Rekrystalizacja: Model:"+nucappmod+" Lokacja:"+nucloc+" Ilość:"+numberofnucleons);
                if (mcsteps>0)
                {
                    settingBorder();
                    //jeżeli w każdej iteracji dodajemy nukleony
                    if(nucappmod==1||nucappmod==2||nucappmod==3) {
                        //System.out.println("Rekrystalizacja: Model:"+nucappmod+" Lokacja:"+nucloc+" Ilość:"+numberofnucleons);
                        for (int i=0;i<numberofnucleons;i++) {
                            boolean again = false;
                            int x;
                            int y;
                            int callscount=0;
                            do {
                                x = rng.nextInt(width - 2);
                                y = rng.nextInt(height - 2);
                                if (nucloc == 1) again = !map[y + 1][x + 1].border;
                                else again = false;
                               // System.out.println("SEEDID: "+map[y+1][x+1].seedid+" RECR:"+map[y+1][x+1].recr);
                                //

                            } while (again||map[y+1][x+1].recr);
                            map[y+1][x+1].seedid=nuclid;
                            newmap[y+1][x+1].seedid=nuclid;
                            map[y + 1][x + 1].recr = true;
                            newmap[y + 1][x + 1].recr = true;
                            int czynnikczer=rng.nextInt(255);
                            seedcolormap.put(map[y + 1][x + 1].seedid, Color.rgb(czynnikczer, 0, 0));
                            map[y + 1][x + 1].setColor(Color.rgb(czynnikczer, 0, 0));
                            newmap[y + 1][x + 1].setColor(Color.rgb(czynnikczer, 0, 0));
                            map[y+1][x+1].energy=0;
                            newmap[y+1][x+1].energy=0;
                            nuclid--;

                        }
                        if (nucappmod==2) numberofnucleons+=numberofnucleons;
                        if (nucappmod==3) numberofnucleons*=0;

                    }
                    Map<Integer, int[]> allspace = new HashMap<Integer, int[]>();
                    int c = -1;

                    for (int i = 1; i < height - 1; i++) {
                        for (int j = 1; j < width - 1; j++) {
                            c++;
                            int[] t = new int[2];
                            t[0] = i;
                            t[1] = j;
                            allspace.put(c, t);
                        }
                    }
                    do {

                        int temp = rng.nextInt(c + 1);
                        // System.out.println("before get "+c);
                        int t[] = allspace.get(temp);

                        //System.out.println("c: " + c + " " + t[0]);


                        // System.out.println(t[1]);
                        smooramcrec(t[0], t[1]);
                        int tmax[] = allspace.get(c);
                        allspace.put(temp, tmax);
                        allspace.remove(c);

                        c--;
                    } while (!allspace.isEmpty());
                    mcsteps--;

                }
            }


            if (finished()&& inctype == 2 && postinclumade==false) {
                settingBorder();
                for (int i = 0; i < inclusionnum; i++) {

                    int x;
                    int y;
                    do {
                        x = rng.nextInt(width - 2);
                        y = rng.nextInt(height - 2);
//                        System.out.println("("+(y+1)+","+(x+1)+")"+map[y+1][x+1].border);
                    } while (map[y+1][x+1].border == false);


                    map[y + 1][x + 1].setState(true);
                    map[y + 1][x + 1].setdstate(-1);
                    map[y + 1][x + 1].inclusion = true;
                    newmap[y + 1][x + 1].setState(true);
                    map[y + 1][x + 1].setColor(Color.rgb(255, 255, 255));
                    newmap[y + 1][x + 1].setColor(map[y + 1][x + 1].getColor());
                    newmap[y + 1][x + 1].setdstate(-1);
                    newmap[y + 1][x + 1].inclusion = true;
                    x = x + 1;
                    y = y + 1;


                    for (int j = 1; j < height - 1; j++) {
                        for (int q = 1; q < width - 1; q++) {

                            if (incshape==1) {
                                if (sqrt((j - y) * (j - y) + (q - x) * (q - x)) <= sizeOfInclusion) {
                                    map[j][q].setState(true);
                                    map[j][q].setdstate(-1);
                                    map[j][q].inclusion = true;
                                    map[j][q].setColor(Color.rgb(255, 255, 255));
                                    newmap[j][q].setColor(Color.rgb(255, 255, 255));
                                    newmap[j][q].setState(true);
                                    newmap[j][q].setdstate(-1);
                                    newmap[j][q].inclusion = true;
                                }
                            }
                            if (incshape==2)
                            {
                                boolean ytrue=false,xtrue=false;
                                double a=0.5*sizeOfInclusion*sqrt(2);

                                if (j<y)
                                {
                                    if (y-j<=(0.5*a))
                                    {
                                        ytrue=true;
                                    }
                                }
                                else
                                {
                                    if (j-y<=(0.5*a))
                                    {
                                        ytrue=true;
                                    }
                                }

                                if (q<x)
                                {
                                    if (x-q<=(0.5*a))
                                    {
                                        xtrue=true;
                                    }
                                }
                                else
                                {
                                    if (q-x<=(0.5*a))
                                    {
                                        xtrue=true;
                                    }
                                }


                                if (xtrue&&ytrue) {
                                    map[j][q].setState(true);
                                    map[j][q].setdstate(-1);
                                    map[j][q].inclusion = true;
                                    map[j][q].setColor(Color.rgb(255, 255, 255));
                                    newmap[j][q].setColor(Color.rgb(255, 255, 255));
                                    newmap[j][q].setState(true);
                                    newmap[j][q].setdstate(-1);
                                    newmap[j][q].inclusion = true;
                                }
                            }
                        }
                    }


                }
                postinclumade=true;
            }
            if (!importing) {
                updateMap();
            }

    }

    private boolean randHex(){
        Random rng=new Random();
        boolean a=rng.nextBoolean();
        return a;
    }

    private int randPenta(){
        Random rng=new Random();
        int a=rng.nextInt(3-0);
        return a;
    }

    public void setNeigh(int neigh) {
        this.neigh = neigh;

        if(neigh==5 || neigh==4) {
            Random rng=new Random();
            choice=rng.nextBoolean();
        }
    }


    public boolean isOnSeedBorder(int i,int j)
    {
        return map[i][j].border;

    }

    public void setPeriodic(boolean periodic) {
        this.periodic = periodic;
    }

    public boolean isPeriodic() {
        return periodic;
    }

    public void setSeedRule(int seedRule){
        this.seedRule = seedRule;
    }
    public void setR(int r){
        this.r = r;
    }
    public void settingBorder(){

        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                map[i][j].border=false;
                Color temp = map[i][j].getColor();
                if (map[i - 1][j - 1].getColor() != temp && !map[i - 1][j - 1].inclusion) {

                    newmap[i][j].border = true;
                    map[i][j].border = true;
                }
                if (map[i - 1][j].getColor() != temp && !map[i - 1][j].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i - 1][j + 1].getColor() != temp && !map[i - 1][j+1].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i][j - 1].getColor() != temp && !map[i][j-1].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i][j + 1].getColor() != temp&& !map[i][j+1].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i + 1][j - 1].getColor() != temp && !map[i + 1][j-1].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i + 1][j].getColor() != temp && !map[i + 1][j].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                if (map[i + 1][j + 1].getColor() != temp && !map[i + 1][j+1].inclusion) {

                    map[i][j].border = true;

                    newmap[i][j].border = true;
                }
                //System.out.println(map[i][j].border);
            }
        }}
        public void select(boolean all,int seedid)        {
            if (all)
            {
                for (int i=1;i<height-1;i++) {
                    for (int j = 1; j < width - 1; j++) {
                        map[i][j].substructure=true;
                        newmap[i][j].substructure=true;
                        map[i][j].selected=true;
                        newmap[i][j].selected=true;
                    }
                }
            }
            else
            {
                for (int i=1;i<height-1;i++) {
                    for (int j = 1; j < width - 1; j++) {
                        if (map[i][j].seedid==seedid) {
                            map[i][j].substructure = true;
                            newmap[i][j].substructure = true;
                            map[i][j].selected = true;
                            newmap[i][j].selected = true;
                        }
                    }
                }
            }
        }
    public void settingBordersize(int size,boolean onlyselected){
        boolean execute=false;
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                map[i][j].border = false;
                Color temp = map[i][j].getColor();
                if (onlyselected) {
                    if (map[i][j].selected) {
                        execute = true;
                    } else {
                        execute = false;
                    }
                } else {
                    execute = true;
                }
                if (execute) {
                    if (map[i - 1][j - 1].getColor() != temp && !map[i - 1][j - 1].inclusion) {


                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);

                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i - 1][j].getColor() != temp && !map[i - 1][j].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i - 1][j + 1].getColor() != temp && !map[i - 1][j + 1].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i][j - 1].getColor() != temp && !map[i][j - 1].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i][j + 1].getColor() != temp && !map[i][j + 1].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i + 1][j - 1].getColor() != temp && !map[i + 1][j - 1].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);

                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i + 1][j].getColor() != temp && !map[i + 1][j].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    if (map[i + 1][j + 1].getColor() != temp && !map[i + 1][j + 1].inclusion) {

                        for (int q = 0; q < size && (j + q) < width - 1; q++) {
                            newmap[i][j + q].border = true;
                            map[i][j + q].border = true;
                            newmap[i][j + q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (j - q) > 1; q++) {
                            newmap[i][j - q].border = true;
                            map[i][j - q].border = true;
                            newmap[i][j - q].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i + q) < height - 1; q++) {
                            newmap[i + q][j].border = true;
                            map[i + q][j].border = true;
                            newmap[i + q][j].setColor(Color.BLACK);


                        }
                        for (int q = 0; q < size && (i - q) > 1; q++) {
                            newmap[i - q][j].border = true;
                            map[i - q][j].border = true;
                            newmap[i - q][j].setColor(Color.BLACK);


                        }
                    }
                    //System.out.println(map[i][j].border);
                }
            }
        }}

        public void substructure(int seedid,boolean typesubdual)
        {

            for (int i=1;i<height-1;i++)
            {
                for (int j=1;j<width-1;j++)
                {
                    if (seedid==map[i][j].seedid)
                    {
                        map[i][j].substructure=true;
                        map[i][j].seedid=-2*seedid;
                        newmap[i][j].substructure=true;
                        newmap[i][j].seedid=-2*seedid;
                        if (typesubdual)
                        {
                            if (substructcolor==null)
                            {
                                Random rng=new Random();
                                do{
                                    substructcolor=Color.rgb(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
                                }while (seedcolormap.containsValue(substructcolor));
                                seedcolormap.put(-2,substructcolor);
                            }
                            map[i][j].setColor(substructcolor);
                            newmap[i][j].setColor(substructcolor);
                        }
                    }

                }
            }

        }
        public void clearallexceptsubstructure()
        {
           // System.out.println("Wchodze");
            for (int i=1;i<height-1;i++)
            {
                for (int j=1;j<width-1;j++)
                {
                    if (!map[i][j].substructure)
                    {
                     //   System.out.println("Zmieniam "+i+" "+j);
                        seedcolormap.remove(map[i][j].seedid,map[i][j].getColor());
                        map[i][j].seedid=0;
                        map[i][j].setState(false);
                        map[i][j].inclusion=false;
                        map[i][j].border=false;
                        map[i][j].setColor(Color.BLACK);
                        newmap[i][j].seedid=0;
                        newmap[i][j].setState(false);
                        newmap[i][j].inclusion=false;
                        newmap[i][j].border=false;
                        newmap[i][j].setColor(Color.BLACK);

                    }
                    else
                    {
                       // System.out.println("Niestety jest substrukturą "+i+" "+j);
                    }
                }
            }
        }
        public void clearspace()
        {
            for (int i=1;i<height-1;i++)
            {
                for (int j=1;j<width-1;j++)
                {
                    if (!map[i][j].selected)
                    {
                        seedcolormap.remove(map[i][j].seedid);
                        map[i][j].seedid=0;
                        map[i][j].setState(false);
                        map[i][j].inclusion=false;
                        map[i][j].border=false;
                        map[i][j].setColor(Color.WHITE);
                        newmap[i][j].seedid=0;
                        newmap[i][j].setState(false);
                        newmap[i][j].inclusion=false;
                        newmap[i][j].border=false;
                        newmap[i][j].setColor(Color.WHITE);
                        map[i][j].substructure=false;
                        newmap[i][j].substructure=false;

                    }
                    else
                    {
                        if (!map[i][j].border)
                        {
                            seedcolormap.put(map[i][j].seedid,Color.WHITE);
                            map[i][j].setColor(Color.WHITE);
                            newmap[i][j].setColor(Color.WHITE);
                        }
                    }
                }
            }

        }

        public void createImage(int appWidth, int appHeight) throws IOException {

            final BufferedImage res = new BufferedImage( height, width, BufferedImage.TYPE_INT_RGB );
            for (int i = 0; i <appHeight; i++) {

                for (int j = 0; j < appWidth; j++) {
                    java.awt.Color awtColor = new java.awt.Color((float) map[j+1][i+1].getColor().getRed(),
                            (float) map[j+1][i+1].getColor().getGreen(),
                            (float) map[j+1][i+1].getColor().getBlue(),
                            (float) map[j+1][i+1].getColor().getOpacity());
                    res.setRGB(i,j,awtColor.getRGB());


                }

            }
            RenderedImage rendImage = res;
            ImageIO.write(rendImage, "bmp", new File("bitmapa.bmp"));
        }
    public void ReadImage(int appWidth, int appHeight) throws IOException {

        int seedids=1;
        File bmpFile = new File("bitmapa.bmp");
        BufferedImage img=ImageIO.read(bmpFile);
        System.out.println("Start importu");
        for (int i = 0; i <appHeight; i++) {

            for (int j = 0; j < appWidth; j++) {

                java.awt.Color color=new java.awt.Color(img.getRGB(i,j));

                map[j+1][i+1].setColor(Color.rgb(color.getRed(),color.getGreen(),color.getBlue()));
                newmap[j+1][i+1].setColor(Color.rgb(color.getRed(),color.getGreen(),color.getBlue()));
                if (seedcolormap.containsValue(Color.rgb(color.getRed(),color.getGreen(),color.getBlue())))
                {
                    map[j+1][i+1].setState(true);
                    map[j+1][i+1].selected=false;
                    map[j+1][i+1].border=false;
                    map[j+1][i+1].substructure=false;
                    newmap[j+1][i+1].setState(true);
                    newmap[j+1][i+1].selected=false;
                    newmap[j+1][i+1].border=false;
                    newmap[j+1][i+1].substructure=false;


                    for (Map.Entry<Integer, Color> entry : seedcolormap.entrySet()) {
                        if (Objects.equals(Color.rgb(color.getRed(),color.getGreen(),color.getBlue()), entry.getValue())) {
                            map[j+1][i+1].seedid=entry.getKey();
                            newmap[j+1][i+1].seedid=entry.getKey();
                        }
                    }
                }
                else
                {
                    map[j+1][i+1].setState(true);
                    map[j+1][i+1].selected=false;
                    map[j+1][i+1].border=false;
                    map[j+1][i+1].substructure=false;
                    seedcolormap.put(seedids,Color.rgb(color.getRed(),color.getGreen(),color.getBlue()));
                    map[j+1][i+1].setColor(Color.rgb(color.getRed(),color.getGreen(),color.getBlue()));
                    map[j+1][i+1].setdstate(seedids);
                    newmap[j+1][i+1].setState(true);
                    newmap[j+1][i+1].selected=false;
                    newmap[j+1][i+1].border=false;
                    newmap[j+1][i+1].substructure=false;
                    newmap[j+1][i+1].setColor(Color.rgb(color.getRed(),color.getGreen(),color.getBlue()));
                    newmap[j+1][i+1].setdstate(seedids);

                    seedids++;
                }


            }

        }

System.out.println("Finished import");
    }

}
